import { createContext, useState, useContext } from "react";
import axios from "../api/axios";

export const AuthContext = createContext(null);

export const getSessionId = () => {
  const { state } = useContext(AuthContext);
  return state.sessionId;
};

const initialState = {
  isLoggedIn: false,
  sessionId: null,
  isAdmin: false,
  email: null,
};

export function AuthProvider({ children }) {
  const [state, setState] = useState(initialState);

  const setIsLoggedIn = (isLoggedIn) =>
    setState((prevState) => ({ ...prevState, isLoggedIn }));
  const setSessionId = (sessionId) =>
    setState((prevState) => ({ ...prevState, sessionId }));
  const setIsAdmin = (isAdmin) =>
    setState((prevState) => ({ ...prevState, isAdmin }));
  const setEmail = (email) =>
    setState((prevState) => ({ ...prevState, email }));

  const login = async (email, password) => {
    const body = { email: email, password: password };
    await axios
      .post("/login", JSON.stringify(body), {
        headers: { "Content-Type": "application/json" },
      })
      .then((response) => {
        const sessionId = response?.data?.data?.sessionId;
        const isAdmin = response.data.data.isAdmin;
        setIsAdmin(isAdmin);
        setIsLoggedIn(true);
        setSessionId(sessionId);
        setEmail(email);
      });
  };

  const logout = () => {
    setIsLoggedIn(false);
    setSessionId(null);
  };

  return (
    <AuthContext.Provider value={{ state, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
}
