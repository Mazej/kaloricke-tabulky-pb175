import NutrientsTable from "./NutrientsTable";
import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { Grid, Typography } from "@mui/material";

export default function FoodDetails({ record, handleClose, open }) {
  console.log(record);
  const foodData = {
    name: record.food.name,
    calories: record.food.calories * record.quantity,
    fats: record.food.fats * record.quantity,
    carbs: record.food.carbs * record.quantity,
    proteins: record.food.proteins * record.quantity,
    fiber: record.food.fiber * record.quantity,
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5">{foodData.name}</Typography>
          </Grid>
          <Grid item xs={12}>
            <NutrientsTable {...foodData} />
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
}
