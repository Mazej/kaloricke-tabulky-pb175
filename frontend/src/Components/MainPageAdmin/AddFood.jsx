import * as React from "react";
import TextField from "@mui/material/TextField";
import { CssBaseline, Grid, Box, Container } from "@mui/material";
import { Typography, Button, Alert } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { AuthContext } from "../../context/AuthContext";
import axios from "../../api/axios";

const theme = createTheme();

const AddFood = () => {
  const { state } = useContext(AuthContext);
  const sessionId = state.sessionId;
  const [alertMessage, setAlertMessage] = useState(null);
  const [alertSeverity, setAlertSeverity] = useState(null);

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm();

  const onSubmit = (_) => {
    setAlertSeverity(null);
    setAlertMessage(null);
    try {
      const body = {
        sessionId: sessionId,
        data: {
          name: getValues("name"),
          /* All values are divided by 100 because user inserts them per 100 grams of food
          but database stores them per 1 gram of food. */
          calories: getValues("calories") / 100,
          fats: getValues("fats") / 100,
          proteins: getValues("proteins") / 100,
          carbs: getValues("carbs") / 100,
          fiber: getValues("fiber") / 100,
        },
      };
      axios
        .put("/food", JSON.stringify(body), {
          headers: { "Content-Type": "application/json" },
        })
        .then((_) => {
          setAlertSeverity("success");
          setAlertMessage("Food successfully added");
          reset();
        })
        .catch((err) => {
          setAlertSeverity("error");
          setAlertMessage(err.response.data.message);
        });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Add food to database
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="name"
                  label="Food name"
                  name="name"
                  {...register("name", { required: "Field is requiered" })}
                  error={!!errors?.name}
                  helperText={errors?.name ? errors.name.message : null}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography component="h1" variant="h6">
                  Nutrients per 100 g
                </Typography>
                <TextField
                  required
                  fullWidth
                  name="calories"
                  label="Calories"
                  type="number"
                  id="calories"
                  {...register("calories", { required: "Field is requiered" })}
                  error={!!errors?.calories}
                  helperText={errors?.calories ? errors.calories.message : null}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="fats"
                  label="Fats"
                  type="number"
                  id="fats"
                  {...register("fats", { required: "Field is requiered" })}
                  error={!!errors?.fats}
                  helperText={errors?.fats ? errors.fats.message : null}
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="carbs"
                label="Carbs"
                type="number"
                id="carbs"
                {...register("carbs", { required: "Field is requiered" })}
                error={!!errors?.carbs}
                helperText={errors?.carbs ? errors.carbs.message : null}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="proteins"
                label="Protein"
                type="number"
                id="protein"
                {...register("proteins", { required: "Field is requiered" })}
                error={!!errors?.proteins}
                helperText={errors?.proteins ? errors.proteins.message : null}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="fiber"
                label="Fiber"
                type="number"
                id="fiber"
                {...register("fiber", { required: "Field is requiered" })}
                error={!!errors?.fiber}
                helperText={errors?.fiber ? errors.fiber.message : null}
              />
            </Grid>
            {alertSeverity && (
              <Alert severity={alertSeverity}>{alertMessage}</Alert>
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Save
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
};
export default AddFood;
