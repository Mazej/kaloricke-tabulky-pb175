import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import axios from "../api/axios";
import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";
import { Alert } from "@mui/material";
import { useForm } from "react-hook-form";

const theme = createTheme();

export default function Registration() {
  const navigateTo = useNavigate();
  const [alertMessage, setAlertMessage] = useState(null);
  const [alertSeverity, setAlertSeverity] = useState(null);
  const {
    watch,
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm();

  function onSubmit(event) {
    try {
      const body = {
        email: getValues("email"),
        password: getValues("password"),
      };
      axios
        .put("/register", JSON.stringify(body), {
          headers: { "Content-Type": "application/json" },
        })
        .then((response) => {
          navigateTo("/login");
        })
        .catch((err) => {
          setAlertSeverity("error");
          setAlertMessage(err.response.data.message);
        });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Registration
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  {...register("email", { required: "Field is requiered" })}
                  error={!!errors?.email}
                  helperText={errors?.email ? errors.email.message : null}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  {...register("password", { required: "Field is requiered" })}
                  error={!!errors?.password}
                  helperText={errors?.password ? errors.password.message : null}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="passwordAgain"
                  label="Password again"
                  type="password"
                  id="passwordAgain"
                  {...register("passwordAgain", {
                    required: "Field is requiered",
                    validate: (val) => {
                      if ((watch("password") || "") != val) {
                        return "Your passwords do no match";
                      }
                    },
                  })}
                  error={!!errors?.passwordAgain}
                  helperText={
                    errors?.passwordAgain ? errors.passwordAgain.message : null
                  }
                />
              </Grid>
            </Grid>
            {alertSeverity && (
              <Alert severity={alertSeverity}>{alertMessage}</Alert>
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Register
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link to="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
