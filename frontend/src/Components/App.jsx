import * as React from "react";
import MainPage from "./MainPage/MainPage";
import Login from "./Login";
import { AuthContext } from "../context/AuthContext";
import { useContext } from "react";
import Registration from "./Registration";
import MainPageAdmin from "./MainPageAdmin/MainPageAdmin";
import TopBar from "./TopBar";

export default function App() {
  const { state } = useContext(AuthContext);
  return !state.isLoggedIn ? (
    <Login />
  ) : (
    <>
      <TopBar /> {state.isAdmin ? <MainPageAdmin /> : <MainPage />}
    </>
  );
}
