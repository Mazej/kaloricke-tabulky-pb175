import * as React from "react";
import Diary from "./Diary";
import Container from "@mui/material/Container";
import AddRecordModal from "./AddRecordModal";
import { AuthContext } from "../../context/AuthContext";
import { useContext, useState, useEffect } from "react";
import axios from "../../api/axios";
import Statistics from "./Statistics";
import {
  Box,
  Button,
  CssBaseline,
  Grid,
  TextField,
  Typography,
} from "@mui/material";

export default function MainPage() {
  const [date, setDate] = useState(new Date().toISOString().split("T")[0]);
  const [showAddRecordModal, setShowAddRecordModal] = useState(false);
  const { state } = useContext(AuthContext);
  const sessionId = state.sessionId;
  const [records, setRecords] = useState([]);

  const fetchRecords = (recordsDate = date) => {
    try {
      console.log("sessionId:", sessionId);
      const body = { sessionId: sessionId, date: recordsDate };
      axios
        .post("/user/diary", JSON.stringify(body), {
          headers: { "Content-type": "application/json" },
        })
        .then((response) => {
          console.log(response);
          setRecords(response.data.data);
        });
    } catch (e) {
      console.log(e);
    }
  };

  const handleModalClose = () => {
    setShowAddRecordModal(false);
    fetchRecords();
  };

  useEffect(() => {
    fetchRecords();
  }, []);

  return (
    <>
      <Container maxWidth="md">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <TextField
                id="date"
                label="Date"
                type="date"
                value={date}
                sx={{ width: 220 }}
                onChange={(e) => {
                  setDate(e.target.value);
                  fetchRecords(e.target.value);
                }}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item xs={7}>
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h5">Diary</Typography>
                <Button
                  onClick={() => setShowAddRecordModal(true)}
                  variant="contained"
                >
                  Add food
                </Button>
              </Box>
              <Diary records={records} />
            </Grid>
            <AddRecordModal
              date={date}
              open={showAddRecordModal}
              handleClose={handleModalClose}
            />
            <Grid item xs={5}>
              <Statistics records={records} />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
}
