import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Typography } from "@mui/material";

export default function Statistics({ records }) {
  let calories = 0;
  let carbs = 0;
  let fats = 0;
  let proteins = 0;
  let fiber = 0;

  console.log(records);
  records.forEach((record) => {
    calories += record.food.calories * record.quantity;
    carbs += record.food.carbs * record.quantity;
    fats += record.food.fats * record.quantity;
    proteins += record.food.proteins * record.quantity;
    fiber += record.food.fiber * record.quantity;
  });

  return (
    <>
      <Typography variant="h5">Statistics</Typography>
      <TableContainer component={Paper}>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Calories</TableCell>
              <TableCell align="right">{Math.round(calories)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Nutrient</TableCell>
              <TableCell align="right">Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {"Carbs"}
              </TableCell>
              <TableCell align="right">{Math.round(carbs)}</TableCell>
            </TableRow>
            <TableRow
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {"Fats"}
              </TableCell>
              <TableCell align="right">{Math.round(fats)}</TableCell>
            </TableRow>
            <TableRow
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {"Proteins"}
              </TableCell>
              <TableCell align="right">{Math.round(proteins)}</TableCell>
            </TableRow>
            <TableRow
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {"Fiber"}
              </TableCell>
              <TableCell align="right">{Math.round(fiber)}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
