import * as React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  Paper,
  TableCell,
} from "@mui/material";
import FoodDetails from "../FoodDetails/FoodDetails";
import { useState } from "react";

export default function Diary({ records }) {
  const [openFoodDetails, setOpenFoodDetails] = useState(false);
  const [clickedFoodRecord, setClickedFoodRecord] = useState(null);

  const handleOpenFoodDetails = (record) => {
    setClickedFoodRecord(record);
    setOpenFoodDetails(true);
  };

  const handleCloseFoodDetails = () => {
    setOpenFoodDetails(false);
    setClickedFoodRecord(null);
  };

  console.log(records);
  return (
    <>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Food</TableCell>
              <TableCell align="right">Grams</TableCell>
            </TableRow>
          </TableHead>
          <TableBody></TableBody>
          {records.map((record) => (
            <TableRow
              onClick={() => handleOpenFoodDetails(record)}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {record.food.name}
              </TableCell>
              <TableCell align="right">{record.quantity}</TableCell>
            </TableRow>
          ))}
        </Table>
      </TableContainer>
      {clickedFoodRecord != null && (
        <FoodDetails
          open={openFoodDetails}
          handleClose={handleCloseFoodDetails}
          record={clickedFoodRecord}
        />
      )}
    </>
  );
}
