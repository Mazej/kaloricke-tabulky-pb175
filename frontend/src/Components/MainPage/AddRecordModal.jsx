import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import axios from "../../api/axios";
import { useState } from "react";
import Autocomplete from "@mui/material/Autocomplete";
import { Grid, TextField, Typography } from "@mui/material";
import { getSessionId } from "../../context/AuthContext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function BasicModal({ date, open, handleClose }) {
  const [foundFoods, setFoundFoods] = useState([]);
  const [selectedFoodName, setSelectedFoodName] = useState("");
  const sessionId = getSessionId();

  const searchFoodByName = (name) => {
    try {
      axios
        .get(`/food/${name}`, {}, {})
        .then((result) => setFoundFoods(result.data.data));
    } catch (e) {
      console.log(e);
    }
  };
  const saveRecord = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    try {
      const foodId = foundFoods.find((food) => {
        return food.name === selectedFoodName;
      }).id;
      const body = {
        sessionId: sessionId,
        data: {
          foodId: foodId,
          date: date,
          quantity: data.get("grams"),
        },
      };
      axios
        .put("/user/diary", body, {
          headers: { "Content-Type": "application/json" },
        })
        .then((result) => {
          handleClose();
          setSelectedFoodName();
          setGrams();
        });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Box component="form" noValidate onSubmit={saveRecord} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h5">Add food to diary</Typography>
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                value={selectedFoodName}
                onChange={(e, value) => setSelectedFoodName(value)}
                disablePortal
                id="combo-box-demo"
                options={foundFoods.map((food) => food.name)}
                sx={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Food name"
                    onChange={(e) => searchFoodByName(e.target.value)}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="grams"
                label="Grams"
                name="grams"
                type="number"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Save
          </Button>
        </Box>
      </Box>
    </Modal>
  );
}
