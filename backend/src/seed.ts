import prisma from "./client";

const seed = async () => {
  console.log("Seeding database");
  const adminData = {
    password: "123123",
    email: "admin@kaloricketabulky.cz",
    isAdmin: true,
  };

  const userData = {
    password: "1",
    email: "1@1.com",
  };

  const admin = await prisma.user.create({ data: { ...adminData } });
  await prisma.user.create({ data: { ...userData } });
  console.log("\nAdmin:");
  console.log(admin, "\n");

  await prisma.food.create({
    data: {
      name: "Banana",
      proteins: 0.07,
      carbs: 0.6,
      fats: 0.05,
      fiber: 0.1,
      calories: 0.8,
    },
  });
  await prisma.food.create({
    data: {
      name: "Chicken meat",
      proteins: 0.2,
      carbs: 0.01,
      fats: 0.02,
      fiber: 0,
      calories: 1.2,
    },
  });
  await prisma.food.create({
    data: {
      name: "Bread",
      proteins: 0.06,
      carbs: 0.4,
      fats: 0.07,
      fiber: 0.04,
      calories: 2.69,
    },
  });
  await prisma.food.create({
    data: {
      name: "Chocolate",
      proteins: 0.03,
      carbs: 0.4,
      fats: 0.5,
      fiber: 0.03,
      calories: 5.3,
    },
  });
};

seed();
