import express from "express";
import { diaryRecord, food, user } from "./resources";

const api = express();
api.use(express.json());

const cors = require("cors");
api.use(cors());

api.get("/api/food/:name", food.getByName);
// TODO add food
api.put("/api/food", food.store);

// TODO record food
api.put("/api/user/diary", diaryRecord.store);
api.post("/api/user/diary", diaryRecord.getRecordsByDate);

// TODO get DayRecord

// ENDPOINTS FOR LOGIN AND REGISTRATION //
api.put("/api/register", user.register);
api.post("/api/login", user.login);

api.listen(process.env["PORT"] || 3000, () => {
  console.log(
    `Express app is listening at http://localhost:${
      process.env["PORT"] || 3000
    }`
  );
});
