import prisma from "../client";

export const getUserIdFromSessionId = async (sessionId: string | undefined) => {
  if (!sessionId) return null;
  const session = await prisma.sessions.findUnique({
    where: { id: sessionId },
  });
  return session ? session.userId : null;
};
