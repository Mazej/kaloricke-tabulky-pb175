import { Response, Request } from "express";
import { date, number, object, string, ValidationError } from "yup";
import { session } from ".";
import prisma from "../client";
import {
  sendCreatedSuccessfully,
  sendInternalServerError,
  sendNotFound,
  sendSuccess,
  sendValidationError,
} from "./universalResponses";

const recordSchema = object({
  date: date().required(),
  foodId: string().required(),
  quantity: number().required(),
  userId: string().required(),
});

export const store = async (req: Request, res: Response) => {
  try {
    const userId = await session.getUserIdFromSessionId(req.body.sessionId);
    if (!userId) return sendNotFound(res, "Session with given id not found");
    req.body.data.userId = userId;

    const data = await recordSchema.validate(req.body.data);

    await prisma.diaryRecord.create({ data: { ...data } });

    return sendCreatedSuccessfully(res, "food recorded successfully", {});
  } catch (e: any) {
    if (e instanceof ValidationError) {
      return sendValidationError(res, e);
    }
    console.log(e);
    return sendInternalServerError(res);
  }
};

export const getRecordsByDate = async (req: Request, res: Response) => {
  try {
    const userId = await session.getUserIdFromSessionId(req.body.sessionId);
    if (!userId) return sendNotFound(res, "Session with given id not found");

    const recordDate = await date().required().validate(req.body.date);

    const records = await prisma.diaryRecord.findMany({
      where: { userId: userId, date: recordDate },
      include: { food: true },
    });

    return sendSuccess(res, "Records retreived successfully", records);
  } catch (e: any) {
    if (e instanceof ValidationError) {
      return sendValidationError(res, e);
    }
    console.log(e);
    return sendInternalServerError(res);
  }
};
