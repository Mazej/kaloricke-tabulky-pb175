import { object, string, number, ValidationError } from "yup";
import prisma from "../client";
import { Request, Response } from "express";
import {
  sendAuthorizationError,
  sendCreatedSuccessfully,
  sendDuplicateError,
  sendInternalServerError,
  sendNotFound,
  sendSuccess,
  sendValidationError,
} from "./universalResponses";
import { session, user } from ".";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime";
const foodSchema = object({
  name: string().required().trim(),
  calories: number().required(),
  proteins: number().required(),
  carbs: number().required(),
  fats: number().required(),
  fiber: number().required(),
});

export const store = async (req: Request, res: Response) => {
  try {
    const data = await foodSchema.validate(req.body.data);

    const userId = await session.getUserIdFromSessionId(req.body.sessionId);
    if (!userId) return sendNotFound(res, "Session with given id not found");
    if (!(await user.isAdmin(userId)))
      return sendAuthorizationError(res, "User is not authorized to add food");

    const food = await prisma.food.create({ data: { ...data } });
    return sendCreatedSuccessfully(res, "Food created successfully", food);
  } catch (e: any) {
    if (e instanceof ValidationError) {
      return sendValidationError(res, e);
    }
    if (e instanceof PrismaClientKnownRequestError && e.code === "P2002")
      return sendDuplicateError(res, "Food with given name already exists");

    console.log(e);
    return sendInternalServerError(res);
  }
};

export const getByName = async (req: Request, res: Response) => {
  try {
    const foods = await prisma.food.findMany({
      where: {
        name: { contains: req.params["name"] },
      },
    });
    return sendSuccess(res, "Foods retreived successfully", foods);
  } catch (e) {
    return sendInternalServerError(res);
  }
};
