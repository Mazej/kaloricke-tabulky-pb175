import { object, string, ValidationError } from "yup";
import prisma from "../client";
import {
  sendAuthorizationError,
  sendCreatedSuccessfully,
  sendDuplicateError,
  sendInternalServerError,
  sendSuccess,
  sendValidationError,
} from "./universalResponses";
import { Request, Response } from "express";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime";

const credentialsSchema = object({
  email: string().email().required().trim(),
  password: string().required(),
})
  .noUnknown(true)
  .strict();

export const register = async (req: Request, res: Response) => {
  try {
    const data = await credentialsSchema.validate(req.body);

    const result = await prisma.user.create({ data: { ...data } });

    const session = await prisma.sessions.create({
      data: { userId: result.id },
    });

    return sendCreatedSuccessfully(res, "", { sessionId: session.id });
  } catch (e) {
    if (e instanceof ValidationError) {
      return sendValidationError(res, e);
    }
    if (e instanceof PrismaClientKnownRequestError && e.code === "P2002")
      return sendDuplicateError(res, "User with given email already exists");

    console.log(e);
    return sendInternalServerError(res);
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const data = await credentialsSchema.validate(req.body);

    const user = await prisma.user.findUnique({
      where: { email: data.email },
    });

    if (user === null || user.password != data.password)
      return sendAuthorizationError(
        res,
        "User password and email does not match"
      );

    const session = await prisma.sessions.create({
      data: { userId: user.id, isAdmin: user.isAdmin },
    });

    const responseData = {
      sessionId: session.id,
      userId: user.id,
      isAdmin: user.isAdmin,
    };
    return sendSuccess(res, "Login success", responseData);
  } catch (e) {
    if (e instanceof ValidationError) return sendValidationError(res, e);

    console.log(e);
    return sendInternalServerError(res);
  }
};

export const isAdmin = async (userId: string) => {
  const user = await prisma.user.findUnique({ where: { id: userId } });
  return user?.isAdmin;
};
