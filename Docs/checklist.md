# Global

- <input type="checkbox"> Does the code stick to our formatting and code standards? Does running prettier and ESLint over the code should yield no warnings or errors respectively?
- <input type="checkbox"> Does the change re-implement code that would be better served by pulling in a well known module from the ecosystem?
- <input type="checkbox"> Instead of using raw strings, are constants used in the main class? Or if these strings are used across files/classes, is there a static class for the constants?
- <input type="checkbox"> Are magic numbers explained? There should be no number in the code without at least a comment of why it is there. If the number is repetitive, is there a constant/enum or equivalent?

## Naming

- <input type="checkbox"> Do filenames have consistent casing and extension?
- <input type="checkbox"> Do variables/functions/modules have consistent casing?
- <input type="checkbox"> Do variables/functions/modules have descriptive names?

## Code

- <input type="checkbox"> Look for blocks of code with more than a few lines of code that look similar. Is it possible to refactor to reduce duplication?
- <input type="checkbox"> Simplify “too smart” and over-engineered code.
- <input type="checkbox"> Remove unused/unreachable code.
- <input type="checkbox"> Remove commented out code.
- <input type="checkbox"> Remove console.logs (unless you have strong motivation why you need it).
- <input type="checkbox"> Remove unnecessary comments: comments that describe the how.
- <input type="checkbox"> Add necessary comments where needed. Necessary comments are comments that describe the why.

# Javascript

- <input type="checkbox"> Are best practices for error handling followed, as well as try catch finally statements?
- <input type="checkbox"> Is a minimum level of logging in place? Are the logging levels used sensible?

## ES6

- <input type="checkbox"> Can you use spread operator?
- <input type="checkbox"> Can you use destructuring?
- <input type="checkbox"> Change all var to let or const (prefer const)
- <input type="checkbox"> Be consistent in your usage of arrow function

# TypeScript

- <input type="checkbox"> Does TypeScript code compile without raising linting errors?
- <input type="checkbox"> Is there a proper /\* \*/ in the various classes and methods?
- <input type="checkbox"> Are unit tests used where possible? In most cases, tests should be present for APIs, interfaces with data access, transformation, backend elements and models. Ponicode can help with test generation. Ponicode creates test files using Jest syntax.
- <input type="checkbox"> Is a minimum level of logging in place? Is the logging level is the right one?
- <input type="checkbox"> Are heavy operations implemented in the backend, leaving the controller as thin as possible?
- <input type="checkbox"> Is event handling on the html efficiently done?

# React

- <input type="checkbox"> Minimize logic in the render method.
- <input type="checkbox"> Move all code with side effects (for example Ajax calls) out from the render methods
- <input type="checkbox"> Check if you can split up React components to reduce duplication. React components should be small and follow single responsibility principle
- <input type="checkbox"> If using ES6, are you doing destructuring of props?
- <input type="checkbox"> Don’t use mixins, prefer HOC and composition
- <input type="checkbox"> Make sure you don’t edit props within a component (they are read-only)
